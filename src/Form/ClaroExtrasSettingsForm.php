<?php

namespace Drupal\claro_extras\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ClaroExtrasSettingsForm.
 */
class ClaroExtrasSettingsForm extends ConfigFormBase {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'claro_extras.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'claro_extras_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('claro_extras.settings');
    $form['claro_extras_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Claro extras settings'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];

    $form['claro_extras_settings']['tabs'] = [
      '#type' => 'vertical_tabs',
      '#default_tab' => 'basic_tab',
    ];

    $form['claro_extras_settings']['basic_tab']['basic_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Display settings'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#group' => 'tabs',
    ];

    $form['claro_extras_settings']['basic_tab']['basic_settings']['node_form_meta'] = [
      '#type' => 'item',
      '#markup' => '<div class="theme-settings-title">' . $this->t("Node form meta block position") . '</div>',
    ];

    $node_form_meta_default_value = $config->get('node_form_meta') ?? NULL;
    $form['claro_extras_settings']['basic_tab']['basic_settings']['node_form_meta'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display block meta as vertical tabs'),
      '#description'   => $this->t('Use the checkbox to display the node meta block as vertical tabs and under the main node form.'),
      '#default_value' => $node_form_meta_default_value,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    ];

    $form['claro_extras_settings']['basic_tab']['basic_settings']['node_breadcrumbs'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Node Edit Breadcrumb'),
      '#description'   => $this->t("Enable this if breadcrumbs show an erroneous 'node' breadcrumb link when editing a node."),
      '#default_value' => $config->get('node_breadcrumbs'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    ];

    $form['claro_extras_settings']['basic_tab']['basic_settings']['enhance_paragraph_titles'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enhance Paragraph titles'),
      '#description'   => $this->t('Improved Paragraph titles display.'),
      '#default_value' => $config->get('enhance_paragraph_titles'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    ];

    $node_type = $this->entityTypeManager->getStorage('node_type');

    $options = array_map(function ($node_type) {
      return $node_type->label();
    }, $node_type->loadMultiple()
    );
    $default_value = $config->get('node_form_meta_types') ?? [];
    $form['claro_extras_settings']['basic_tab']['basic_settings']['node_form_meta_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Select the content types'),
      '#description'   => $this->t('Select the content type on which display the node meta block as vertical tabs and under the main node form.'),
      '#default_value' => $default_value ? array_filter($default_value) : [],
      '#options' => $options,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="node_form_meta"]' => ['checked' => TRUE],
        ],
      ],
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $values = $form_state->getValues();
    $claro_extras_config = $this->config('claro_extras.settings');
    theme_settings_convert_to_config($values, $claro_extras_config)->save();
  }

}
