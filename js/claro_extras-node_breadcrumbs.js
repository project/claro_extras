/**
 * @file
 * Javascript behaviors for breadcrumb menu when using Claro.
 */

(function ($, Drupal, drupalSettings) {
  const breadcrumbLinks = document.querySelectorAll('.breadcrumb__list li a')

  for (let i = 0; i < breadcrumbLinks.length; i++) {
    if (breadcrumbLinks[i].href.includes("node")) {
      breadcrumbLinks[i].href = drupalSettings.path.baseUrl + "admin/content";
      breadcrumbLinks[i].innerHTML = Drupal.t("Content");
    }
  }
})(jQuery, Drupal, drupalSettings);
